# Stub Project - bit media / Docker Deployment
This project describes the deployment requirements for dev projects.
(c) 2018 by Michael Holasek, bit media e-solutions Gmbh

## Rules
1. Use Tags for deployment
2. Create .gitlab.ci including tag build
3. Create a single stack docker-compose file to test the whole application in a lab environment


## 1. Use Tags for deployment
Create tags if you want to deliver new version. We use semantic versioning (https://semver.org/) 
Version tags must start with v. Gitlab runner should build v tags automatically via gitlab.ci script

Example for development version: v0.1.3

If necessary you can add some suffix to the version for alpha beta or release candidate
Example: v2.3.1-beta1, v2.3.1-rc2


## 2. Create .gitlab-ci.yml including tag build
See example in this project. The ci file should run through all necessary tasks to build a final docker container for all parts from this project
except the other containers. If necessary add the needed docker files to build the correct images (for foreign services as well) - See example in haproxy

Consider the .gitlab-ci.yml file in this project. The pipeline will create artifacts with docker-compose and a docker container with the "app".
You can download the artifacts, extract them, and run "docker-compose up -d" and http://localhost in browser

https://about.gitlab.com/features/gitlab-ci-cd/


## 3. Create a single stack docker-compose file to test the whole application in a lab environment
Implement all needed services in one docker-compose file including the external services (e.g. keycloak, database, redis, message queue, proxy)
We call this "Single Stack Installation". Please see the sample docker-compose file.


# How to test this sample?
1. Download the builded artefact from gitlab pipline (zip contains docker-compose.yml + needed subfolder) --> https://gitlab.com/bitmedia/open-source/project-stub/pipelines
2. Extract the zip somewhere
3. Login to gitlab registry: ```docker login registry.gitlab.com```
4. On Docker for Windows use powershell and run ```$Env:COMPOSE_CONVERT_WINDOWS_PATHS=1``` before docker-compose up -d 
5. Start the stack ```docker-compose -p project-stub up -d```
6. Open the services with the specific urls (see docker-compose comments)