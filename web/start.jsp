<html>
<head>
<title>Tomcat - Postgre Docker Sample</title>
<%@ page language="java" import="java.sql.*" %>
</head>
<body>

<%-- START --%>
<%
    out.println("Sample JSP - Tomcat --> Postgresql using Docker");

Connection connection = null;
PreparedStatement ps = null;

Class.forName("org.postgresql.Driver");
connection = DriverManager.getConnection(
   "jdbc:postgresql://db:5432/app_demo","postgres", "secure");


String sql = "SELECT * FROM people";
ps = connection.prepareStatement(sql);
ResultSet rs = ps.executeQuery();
connection.close();

%>
<%-- END --%>

<TABLE BORDER="1">
            <TR>
                <TH>ID</TH>
                <TH>Firstname</TH>
                <TH>Lastname</TH>
            </TR>
            <% while(rs.next()){ %>
            <TR>
                <TD> <%= rs.getString(1) %></td>
                <TD> <%= rs.getString(2) %></TD>
                <TD> <%= rs.getString(3) %></TD>
            </TR>
            <% } %>
        </TABLE>

</body>
</html>