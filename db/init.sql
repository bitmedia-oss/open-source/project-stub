DROP TABLE IF EXISTS people;
CREATE TABLE people (
    id SERIAL PRIMARY KEY,
    firstname VARCHAR(64),
    lastname VARCHAR(64)
);
INSERT INTO people (firstname, lastname) VALUES ('Louis', 'Martin Simoneau');
INSERT INTO people (firstname, lastname) VALUES ('Albert', 'Einstein' );
INSERT INTO people (firstname, lastname) VALUES ('Marie', 'Curie');
INSERT INTO people (firstname, lastname) VALUES ('Joe', 'Louis');
INSERT INTO people (firstname, lastname) VALUES ('Marilyn', 'Monroe');