#!/bin/sh
# generate self signed ssl cert only if all cert files are empty or nonexistent

    mkdir -p /etc/ssl
    cd /etc/ssl

    # Generating signing SSL private key
    openssl genrsa -des3 -passout pass:x -out key.pem 2048

    # Removing passphrase from private key
    cp key.pem key.pem.orig
    openssl rsa -passin pass:x -in key.pem.orig -out key.pem

    # Generating certificate signing request
    openssl req -new -key key.pem -out cert.csr -subj "/C=AT/ST=STMK/L=Graz/O=bit media/OU=ICT/CN=esp-idm.docker.localhost"

    # Generating self-signed certificate
    openssl x509 -req -days 3650 -in cert.csr -signkey key.pem -out cert.pem

    cat cert.pem key.pem > selfsigned-cert.pem